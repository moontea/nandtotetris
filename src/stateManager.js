//store creator
export const createStore = reducer => {
    //only let in the entire app!
    let state = {};

    const dispatch = action => {
        state = reducer(state, action);
    };

    const getState = () => ({ ...state });

    return { getState, dispatch };
};

//logic specific reducer
export const reducer = (state, action) => {
    switch (action.type) {
        case 'init':
            return {
                ...action.initState,
            };
        case 'ADD_ENTRY': {
            return {
                ...state,
                [action.symbolName]: action.address,
            };
        }
        default:
            return state;
    }
};

//logic specific actions / action creators
export const makeInit = initState => ({
    type: 'init',
    initState,
});

export const addEntry = (symbolName, address) => ({
    type: 'ADD_ENTRY',
    symbolName,
    address,
});
