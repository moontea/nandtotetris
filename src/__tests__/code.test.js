import * as code from '../code.js';

describe('Code', () => {
    describe('utils', () => {
        test('Remove Comments, removes all text after // and remove //', () => {
            expect(code.removeComment(' // comment')).toBe(' ');
        });
    });
});
