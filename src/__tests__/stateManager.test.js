import { createStore, reducer, makeInit } from '../stateManager.js';

describe('State Manager', function() {
    it('it should createStore based on a passed in reducer function', function() {
        const store = createStore({}, () => {});
        expect(typeof store.getState).toEqual('function');
        expect(typeof store.dispatch).toEqual('function');
    });

    describe('File Reducer branches', function() {
        const defaultState = {};

        it('should ignore bad commands', function() {
            const action = { type: 'does_not_exist' };
            const result = reducer({}, action);
            expect(result).toEqual({});
        });

        it('should init/reset the game state', function() {
            const action = {
                type: 'init',
                initState: defaultState,
            };

            const result = reducer(undefined, action);

            expect(result).toEqual(defaultState);
        });

        /*it("should place pawn given name, location", function() {
      const pawnData = {
        name: "R2-D2",
        position: {
          x: 25,
          y: 25
        }
      };

      const action = {
        type: "place",
        ...pawnData,
        inMapExtents: () => true
      };

      const expectedState = {
        ...defaultState,
        pawns: defaultState.pawns.map(pawn =>
          pawn.name === pawnData.name ? { ...pawn, ...pawnData } : pawn
        )
      };

      const result = reducer(defaultState, action);

      expect(result).to.deep.equal(expectedState);
    });

    it("should NOT place pawn if pawn is not in extents", function() {
      const pos = { x: -1, y: 0 };
      const pawnData = {
        name: "R2-D2",
        position: pos
      };

      const action = {
        type: "place",
        ...pawnData,
        inMapExtents: () => false
      };

      const result = reducer(defaultState, action);

      expect(result).to.deep.equal(defaultState);
    });

    it("should face the pawn", function() {
      const pawnData = {
        name: "R2-D2",
        facing: "something"
      };

      const action = {
        type: "face",
        ...pawnData
      };

      const expectedState = {
        ...defaultState,
        pawns: defaultState.pawns.map(pawn =>
          pawn.name === pawnData.name ? { ...pawn, ...pawnData } : pawn
        )
      };

      const result = reducer(defaultState, action);

      expect(result).to.deep.equal(expectedState);
    });
    */
    });

    describe('Action Creators ', function() {
        it('should make an init', function() {
            const action = makeInit({});
            expect({ type: 'init', initState: {} }).toEqual(action);
        });
        /*
    it("should make a place", function() {
      const name = "some";
      const position = { x: 0, y: 0 };
      const inMapExtents = () => {};

      const action = makePlace({ name, position, inMapExtents });
      expect({ type: "place", name, position, inMapExtents }).to.deep.equal(
        action
      );
    });

    it("should make a face", function() {
      const name = "some";
      const facing = "thing";

      const action = makeFace({ name, facing });
      expect({ type: "face", name, facing }).to.deep.equal(action);
    });
    */
    });
});
