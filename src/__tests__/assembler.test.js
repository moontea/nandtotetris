import * as assembler from '../assembler';

describe('assembler', () => {
    /*
	describe('Contructor', () => {
		test('Opens the input file / stream and gets ready to parse it.', () => {
			//const state = assembler.create("fileName");
			//expect(state.fileContent).toBe("the commands");
			//expect(state.currentLineNumber).toBe(0);
			//expect(assembler.currentLine(state)).toBe("A=D+M");
			expect(true).toBe(false);
		});
	});

	describe('hasMoreCommands -- asre there more commands in the input?', () => {
		test('expect hasMoreCommands to be true at the beginning of a file', () => {
			//const state = assembler.create("fileName");
			//expect(myassembler.hasMoreCommands).toBe(true);
			expect(true).toBe(false);
		});

		test('expect hasMoreCommands to be false at the end of the file', () => {
			//const myassembler = assembler.create("fileName");
			//myassembler.advance(state);
			//expect(myassembler.hasMoreCommands).toBe(false);
			expect(true).toBe(false);
		});
	});
	*/

    describe('commandType -- returns the type of the command', () => {
        test('if @Xxx then A_COMMAND', () => {
            expect(assembler.commandType('@Xxx')).toBe('A_COMMAND');
        });

        test('if @100 then A_COMMAND.', () => {
            expect(assembler.commandType('@100')).toBe('A_COMMAND');
        });

        test('if M=M+1 then C_COMMAND', () => {
            expect(assembler.commandType('M=M+1')).toBe('C_COMMAND');
        });

        test('if M=M then C_COMMAND', () => {
            expect(assembler.commandType('M=M')).toBe('C_COMMAND');
        });

        test('if D;JGT then C_COMMAND', () => {
            expect(assembler.commandType('D;JGT')).toBe('C_COMMAND');
        });

        test('if (Xxx) then L_COMMAND', () => {
            expect(assembler.commandType('(Xxx)')).toBe('L_COMMAND');
        });

        test('if (Xx) then L_COMMAND', () => {
            expect(assembler.commandType('(Xx)')).toBe('L_COMMAND');
        });
    });

    describe('symbol -- returns the sybol in a L_COMMAND or A_COMMAND', () => {
        test('if @100 then returns 100', () => {
            expect(assembler.symbol('@100')).toBe('100');
        });

        test('if @Xxx then Xxx', () => {
            expect(assembler.symbol('@Xxx')).toBe('Xxx');
        });

        test('if (Xxx) then Xxx', () => {
            expect(assembler.symbol('(Xxx)')).toBe('Xxx');
        });
    });

    describe('jump -- returns the jump mnemonic in the current C-command', () => {
        test('if D=M then returns null; there is no jump', () => {
            expect(assembler.jump('D=M')).toBe(null);
        });

        test('if D;JGT then JGT', () => {
            expect(assembler.jump('D;JGT')).toBe('JGT');
        });

        test('if 0;JMP then JMP', () => {
            expect(assembler.jump('0;JMP')).toBe('JMP');
        });
    });

    describe('comp -- returns the comp mnemonic in the current C-command', () => {
        test('if D=M then M', () => {
            expect(assembler.comp('D=M')).toBe('M');
        });

        test('if D=M-D then M-D', () => {
            expect(assembler.comp('D=M-D')).toBe('M-D');
        });

        test('if D;JGT then D', () => {
            expect(assembler.comp('D;JGT')).toBe('D');
        });

        test('if 0;JMP then 0', () => {
            expect(assembler.comp('0;JMP')).toBe('0');
        });
    });

    describe('dest -- returns the dest mnemonic in the current C-command', () => {
        test('if D=M then D', () => {
            expect(assembler.dest('D=M')).toBe('D');
        });

        test('if D=M-D then D', () => {
            expect(assembler.dest('D=M-D')).toBe('D');
        });

        test('if D;JGT then null', () => {
            expect(assembler.dest('D;JGT')).toBe(null);
        });

        test('if 0;JMP then null', () => {
            expect(assembler.dest('0;JMP')).toBe(null);
        });
    });

    describe('destToBinary -- takes a dest mnemonic and returns the proper binary', () => {
        const destHash = {
            null: '000',
            M: '001',
            D: '010',
            MD: '011',
            A: '100',
            AM: '101',
            AD: '110',
            AMD: '111',
        };

        Object.keys(destHash).forEach(key =>
            test(`if ${key} then ${destHash[key]}`, () => {
                expect(assembler.destToBinary(key)).toBe(destHash[key]);
            }),
        );
    });

    describe('destToBinary -- takes a dest mnemonic and returns the proper binary', () => {
        const jumpHash = {
            null: '000',
            JGT: '001',
            JEQ: '010',
            JGE: '011',
            JLT: '100',
            JNE: '101',
            JLE: '110',
            JMP: '111',
        };

        Object.keys(jumpHash).forEach(key =>
            test(`if ${key} then ${jumpHash[key]}`, () => {
                expect(assembler.jumpToBinary(key)).toBe(jumpHash[key]);
            }),
        );
    });

    describe('compToBinary -- takes a comp mnemonic and returns the proper binary', () => {
        //28 0f these
        const compHash = {
            '0': '0101010',
            '1': '0111111',
            '-1': '0111010',
            D: '0001100',
            A: '0110000',
            '!D': '0001101',
            '!A': '0110001',
            '-D': '0001111',
            '-A': '0110011',
            'D+1': '0011111',
            'A+1': '0110111',
            'D-1': '0001110',
            'A-1': '0110010',
            'D+A': '0000010',
            'D-A': '0010011',
            'A-D': '0000111',
            'D&A': '0000000',
            'D|A': '0010101',
            M: '1110000',
            '!M': '1110001',
            '-M': '1110011',
            'M+1': '1110111',
            'M-1': '1110010',
            'D+M': '1000010',
            'D-M': '1010011',
            'M-D': '1000111',
            'D&M': '1000000',
            'D|M': '1010101',
        };

        Object.keys(compHash).forEach(key =>
            test(`if ${key} then ${compHash[key]}`, () => {
                expect(assembler.compToBinary(key)).toBe(compHash[key]);
            }),
        );
    });
    describe('Decimal to Fixed 16 digit Binary', () => {
        test('30 to 16 bit binary is 0000000000011110', () => {
            expect(assembler.decimalTo16FixedBinary('30')).toBe('0000000000011110');
        });
    });
});
