import * as parser from '../parser.js';
import { readFile, stripCommandString, convertLines } from '../code.js';
import { parseInstruction } from '../assembler.js';
//import { createStore, reducer } from '../stateManager';

const testDataPaths = {
    MemoryAccess: ['BasicTest', 'PointerTest', 'StaticTest'],
    StackArithmetic: ['SimpleAdd', 'StackTest'],
};

const baseName = (folder, fileBaseName) => `${folder}/${fileBaseName}/${fileBaseName}`;
const addExtension = (base, extension) => `${base}.${extension}`;

describe('Parser', () => {
    describe('includesOne', () => {
        test('expect sub to be in list', () => {
            expect(parser.includesOne(['sub', 'max'])('sub')).toBe(true);
        });
        test('expect subsandwich to be in list', () => {
            expect(parser.includesOne(['sub', 'max'])('subsandwich')).toBe(true);
        });
        test('expect clubsandwich to NOT be in list', () => {
            expect(parser.includesOne(['sub', 'max'])('clubsandwich')).toBe(false);
        });
    });

    Object.keys(testDataPaths).forEach(folder => {
        testDataPaths[folder].forEach(fileBaseName => {
            describe(`${folder}/${fileBaseName}`, () => {
                const name = baseName(folder, fileBaseName);
                const instructions = readFile(addExtension(name, 'vm'));

                test('commandType', () => {
                    const types = readFile(addExtension(name, 'commandTypes'));
                    const myTypes = instructions.map(parser.commandType);
                    expect(myTypes).toStrictEqual(types);
                });

                test('arg1', () => {
                    const arg1s = readFile(addExtension(name, 'arg1'));
                    const myArg1s = instructions.map(parser.arg1).filter(item => !!item);
                    expect(myArg1s).toStrictEqual(arg1s);
                });

                test('arg2', () => {
                    const arg2s = readFile(addExtension(name, 'arg2'));
                    const myArg2s = instructions.map(parser.arg2).filter(item => !!item);
                    expect(myArg2s).toStrictEqual(arg2s);
                });
            });
        });
    });

    describe('My fist ASM converter', () => {
        test('convert simpleAdd', () => {
            const simpleAdd = readFile(
                addExtension(baseName('StackArithmetic', 'SimpleAdd'), 'vm'),
            );
            const asmFile = parser.linesToAsm('SimpleAdd', simpleAdd);
            //console.log(asmFile);
            //const asmStripped = XXX(asmFile);
            expect(true).toBe(false);
        });

        test('convert simpleAdd', () => {
            const simpleAdd = readFile(
                addExtension(baseName('StackArithmetic', 'StackTest'), 'vm'),
            );
            const asmFile = parser.linesToAsm('StackTest', simpleAdd);
            console.log(asmFile);
            const commands = stripCommandString(asmFile);
            //console.log(commands.map((c, i) => i + ' ' + c).join('\n'));
            const bin = convertLines(commands); //.map(parseInstruction(store));
            //console.log(bin);
            console.log(bin.join('\n'));
            //const asmStripped = XXX(asmFile);
            expect(true).toBe(false);
        });
    });
});
