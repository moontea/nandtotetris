// command parsers
export const commandType = cmd =>
    cmd[0] === '@' ? 'A_COMMAND' : cmd[0] === '(' ? 'L_COMMAND' : 'C_COMMAND';

export const jump = cmd => (cmd.includes(';') ? cmd.split(';')[1] : null);

export const comp = cmd => (cmd.includes('=') ? cmd.split('=')[1] : cmd.split(';')[0]);

export const dest = cmd => (cmd.includes('=') ? cmd.split('=')[0] : null);

export const symbol = cmd =>
    cmd.includes('@') ? cmd.split('@')[1] : cmd.replace('(', '').replace(')', '');

//ToBinaries
export const destToBinary = dest =>
    ({
        null: '000',
        M: '001',
        D: '010',
        MD: '011',
        A: '100',
        AM: '101',
        AD: '110',
        AMD: '111',
    }[dest]);

export const jumpToBinary = jump =>
    ({
        null: '000',
        JGT: '001',
        JEQ: '010',
        JGE: '011',
        JLT: '100',
        JNE: '101',
        JLE: '110',
        JMP: '111',
    }[jump]);

export const compToBinary = comp =>
    (comp.includes('M') ? '1' : '0') +
    {
        '0': '101010',
        '1': '111111',
        '-1': '111010',
        D: '001100',
        A: '110000',
        '!D': '001101',
        '!A': '110001',
        '-D': '001111',
        '-A': '110011',
        'D+1': '011111',
        'A+1': '110111',
        'D-1': '001110',
        'A-1': '110010',
        'D+A': '000010',
        'D-A': '010011',
        'A-D': '000111',
        'D&A': '000000',
        'D|A': '010101',
    }[comp.replace('M', 'A')];

export const decimalTo16FixedBinary = numberAsString => {
    //convert to binary is strange, but here is what you need to do
    const bin = (numberAsString >>> 0).toString(2);
    //console.log(bin);
    return Array(16 - bin.length)
        .fill('0')
        .join('')
        .concat(bin);
};

export const parseInstructionNoSymbols = line =>
    commandType(line) === 'A_COMMAND'
        ? decimalTo16FixedBinary(symbol(line))
        : commandType(line) === 'L_COMMAND'
        ? null
        : `111${compToBinary(comp(line))}${destToBinary(dest(line))}${jumpToBinary(jump(line))}`;

export const parseInstruction = store => line =>
    commandType(line) === 'A_COMMAND'
        ? decimalTo16FixedBinary(
              Number.isInteger(symbol(line)) ? symbol(line) : store.getState()[symbol(line)],
          )
        : commandType(line) === 'L_COMMAND'
        ? null
        : `111${compToBinary(comp(line))}${destToBinary(dest(line))}${jumpToBinary(jump(line))}`;
