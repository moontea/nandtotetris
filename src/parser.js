const commandTypes = [
    'C_ARITHMETIC',
    'C_PUSH',
    'C_POP',
    'C_LABEL',
    'C_GOTO',
    'C_IF',
    'C_FUNCTION',
    'C_RETURN',
    'C_CALL',
].reduce((acc, curr) => ({ ...acc, [curr]: curr }), {});

//Line decomposers
const arithmeticCommands = ['add', 'sub', 'neg', 'eq', 'gt', 'lt', 'and', 'or', 'not'];
export const commandType = line => {
    if (includesOne(arithmeticCommands)(line)) {
        return commandTypes.C_ARITHMETIC;
    }
    if (line.includes('push')) {
        return commandTypes.C_PUSH;
    }
    if (line.includes('pop')) {
        return commandTypes.C_POP;
    }
    if (line.includes('label')) {
        return commandTypes.C_LABEL;
    }
    if (line.includes('goto')) {
        return commandTypes.C_GOTO;
    }
    if (line.includes('if')) {
        return commandTypes.C_IF;
    }
    if (line.includes('function')) {
        return commandTypes.C_FUNCTION;
    }
    if (line.includes('return')) {
        return commandTypes.C_RETURN;
    }
    if (line.includes('call')) {
        return commandTypes.C_CALL;
    }
    return 'ERROR';
};

export const arg1 = line => {
    const type = commandType(line);
    if (type === commandTypes.C_ARITHMETIC) {
        return line;
    }
    if (type === commandTypes.C_RETURN) {
        return undefined;
    }
    return line.split(' ')[1];
};

export const arg2 = line => {
    const type = commandType(line);
    if (
        includesOne(
            [commandTypes.C_POP, commandTypes.C_PUSH, commandTypes.C_FUNCTION, commandTypes.C_CALL],
            type,
        )
    ) {
        return line.split(' ')[2];
    }

    return undefined;
};

//Line Aggregators
export const translateToAsm = (fileName, line, index) => ({
    fileName,
    line,
    index,
    commandType: commandType(line),
    arg1: arg1(line),
    arg2: arg2(line),
});

export const linesToAsm = (fileName, lines) =>
    lines.reduce((acc, line, index) => {
        const lineData = translateToAsm(fileName, line, index);
        const before = acc + printLine(lineData);

        if (lineData.commandType === commandTypes.C_PUSH) {
            if (lineData.arg1 === 'constant') {
                return before + pushConst(lineData);
            }
        }

        if (lineData.commandType === commandTypes.C_ARITHMETIC) {
            return before + ALUCommands[lineData.arg1](lineData);
        }
    }, '           // Generated .asm from .vm');

//utils
export const includesOne = arr => str =>
    arr.reduce((acc, curr) => acc || str.includes(curr), false);

// ASM Conversion
const printLine = lineData => `
           
           // ${lineData.line}`;

const pushConst = lineData => `
    @${lineData.arg2}     // ${lineData.commandType} ${lineData.arg1}
    D=A    // Load the const into D for later
    @SP    // get stack pointer SP
    A=M    // A will get value of M[SP]
    M=D    // Store M[M[SP]] into D
    ${incrementSP}`;

const popStatic = lineData => `
    @SP    // get stack pointer SP
    A=M    // A will get value of M[SP]
    D=M    // Store M[M[SP]] into D
    @${lineData.fileName}.${lineData.arg2}
    M=D    //store popped Data into ${lineData.fileName}.${lineData.arg2}
    ${decrementSP}`;

const decrementSP = `
    @SP    // Decrement Stack Pointer
    M=M-1  // M[SP] will equal M[SP] - 1`;

const binaryStackOp = `${decrementSP}
    
    @SP    
    A=M    // make A equal the number at M[SP]
    D=M    // D will get data at M[M[SP]] (y)
    @SP    // Point to SP
    A=M-1  // A = M[SP]-1 (1 before current stack)`;

const bool = jumpType => lineData => {
    const uniq = `.${lineData.fileName}.${lineData.index}`;
    return `${binaryStackOp}
    D=M-D  // D = M[M[SP]-1](x) - D(y)
    @TRUE${uniq}  // LOAD The address of True, to skip false
    ${jumpType}
    @SP    
    A=M-1  // make A equal the number at M[SP]-1
    M=0    // FALSE is the answer if we reached here
    @ENDIF${uniq} // JUMP past true
    0;JMP  
(TRUE${uniq})     // We jumped here if TRUE
    @SP    
    A=M-1  // make A equal the number at M[SP]-1
    M=-1   // -1 is TRUE
(ENDIF${uniq})    // Place to jump after FALSE`;
};

const incrementSP = `
    @SP    // Increment Stack Pointer
    M=M+1  // M[SP] will equal M[SP] + 1`;

const unaryStackOp = `
    @SP    
    A=M-1  // make A equal the number at M[SP] -1`;

const ALUCommands = {
    add: _ => `${binaryStackOp}
    M=D+M  // M[M[SP]-1](x) = D(y) + M[M[SP]-1](x)`,
    sub: _ => `${binaryStackOp}
    M=M-D  // M[M[SP]-1](x) = D(y) - M[M[SP]-1](x)`,
    neg: _ => `${unaryStackOp}
    M=-M   // M[M[SP]-1](x) = -M[M[SP]-1](x)`,
    eq: bool(`D;JEQ  // IF x - y = 0 then x == y`),
    gt: bool(`D;JGT  // IF x - y = is greater than 0 then x > y`),
    lt: bool(`D;JLT  // IF x - y = is lesser than 0 then x < y`),
    and: _ => `${binaryStackOp}
    M=D&M  // M[M[SP]-1](x) = D(y) & M[M[SP]-1](x)`,
    or: _ => `${binaryStackOp}
    M=D|M  // M[M[SP]-1](x) = D(y) | M[M[SP]-1](x)`,
    not: _ => `${unaryStackOp}
    M=!M   // M[M[SP]-1](x) = !M[M[SP]-1](x)`,
};
