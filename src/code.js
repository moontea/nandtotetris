import fs from 'fs';
import { parseInstructionNoSymbols, parseInstruction, commandType, symbol } from './assembler.js';
import { createStore, reducer, addEntry } from './stateManager.js';

export const removeComment = line => line.split('//')[0];

export const readFile = fileName =>
    fs
        .readFileSync(fileName)
        .toString()
        .split(/\r\n|\n/)
        .reduce((acc, line) => {
            //console.log(acc);
            return [...acc, line];
        }, [])
        .map(removeComment)
        .map(line => line.trim())
        .filter(line => !!line);

//TODO: make this reusable
export const stripCommandString = str =>
    str
        .split(/\r\n|\n/)
        .reduce((acc, line) => {
            //console.log(acc);
            return [...acc, line];
        }, [])
        .map(removeComment)
        .map(line => line.trim())
        .filter(line => !!line);

export const convertFileNoSymbols = fileName => readFile(fileName).map(parseInstructionNoSymbols);

export const writeArrayAsFile = (arr, fileName) =>
    fs.writeFile(fileName, arr.join('\r\n'), err => {
        if (err) {
            console.error('ERROR');
        }
    });

export const translateFile = fileName => `${fileName.slice(0, lastIndexOf('.'))}.hack`;

//Non Pure function, takes a store and does stuff to it.
export const symbolPass = (store, lines) => {
    let instructionCount = 0;
    let nextRamAddress = 0;

    lines.forEach(line => {
        if (commandType(line) === 'L_COMMAND') {
            store.dispatch(addEntry(symbol(line), instructionCount));
        } else {
            instructionCount++;
        }
    });

    lines.forEach(line => {
        if (commandType(line) === 'A_COMMAND') {
            const variable = symbol(line);
            if (!Number.isInteger(variable)) {
                if (!store.getState().hasOwnProperty(variable)) {
                    store.dispatch(addEntry(variable, nextRamAddress));
                    nextRamAddress++;
                }
            }
        }
    });
};

export const convertFile = fileName => {
    //non pure
    const store = createStore(reducer);
    const lines = readFile(fileName);
    //non pure
    symbolPass(store, lines);
    return lines.map(parseInstruction(store)).filter(line => !!line);
};

export const convertLines = lines => {
    //non pure
    const store = createStore(reducer);
    //non pure
    symbolPass(store, lines);
    return lines.map(parseInstruction(store)).filter(line => !!line);
};
