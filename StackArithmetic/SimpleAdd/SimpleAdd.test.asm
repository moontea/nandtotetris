              // Generated .asm from .vm
               
               // push constant 17
        @17     // C_PUSH constant
        D=A    // Load the const into D for later
        @SP    // get stack pointer SP
        A=M    // A will get value of M[SP]
        M=D    // Store M[M[SP]] into D
        
        @SP    // Increment Stack Pointer
        M=M+1  // M[SP] will equal M[SP] + 1
               
               // push constant 17
        @17     // C_PUSH constant
        D=A    // Load the const into D for later
        @SP    // get stack pointer SP
        A=M    // A will get value of M[SP]
        M=D    // Store M[M[SP]] into D
        
        @SP    // Increment Stack Pointer
        M=M+1  // M[SP] will equal M[SP] + 1
               
               // eq
        @SP    // Decrement Stack Pointer
        M=M-1  // M[SP] will equal M[SP] - 1
        
        @SP    
        A=M    // make A equal the number at M[SP]
        D=M    // D will get data at M[M[SP]] (y)
        @SP    // Point to SP
        A=M-1  // A = M[SP]-1 (1 before current stack)
        D=M-D  // D = M[M[SP]-1](x) - D(y)
        @TRUE.StackTest.2  // LOAD The address of True, to skip false
        D;JEQ  // IF x - y = 0 then x == y
        @SP    
        A=M-1  // make A equal the number at M[SP]-1
        M=0    // FALSE is the answer if we reached here
        @ENDIF.StackTest.2 // JUMP past true
        0;JMP  
    (TRUE.StackTest.2)     // We jumped here if TRUE
        @SP    
        A=M-1  // make A equal the number at M[SP]-1
        M=-1   // -1 is TRUE
    (ENDIF.StackTest.2)    // Place to jump after FALSE
               
               // push constant 17
        @17     // C_PUSH constant
        D=A    // Load the const into D for later
        @SP    // get stack pointer SP
        A=M    // A will get value of M[SP]
        M=D    // Store M[M[SP]] into D
        
        @SP    // Increment Stack Pointer
        M=M+1  // M[SP] will equal M[SP] + 1
               
               // push constant 16
        @16     // C_PUSH constant
        D=A    // Load the const into D for later
        @SP    // get stack pointer SP
        A=M    // A will get value of M[SP]
        M=D    // Store M[M[SP]] into D
        
        @SP    // Increment Stack Pointer
        M=M+1  // M[SP] will equal M[SP] + 1
               
               // eq
        @SP    // Decrement Stack Pointer
        M=M-1  // M[SP] will equal M[SP] - 1
        
        @SP    
        A=M    // make A equal the number at M[SP]
        D=M    // D will get data at M[M[SP]] (y)
        @SP    // Point to SP
        A=M-1  // A = M[SP]-1 (1 before current stack)
        D=M-D  // D = M[M[SP]-1](x) - D(y)
        @TRUE.StackTest.5  // LOAD The address of True, to skip false
        D;JEQ  // IF x - y = 0 then x == y
        @SP    
        A=M-1  // make A equal the number at M[SP]-1
        M=0    // FALSE is the answer if we reached here
        @ENDIF.StackTest.5 // JUMP past true
        0;JMP  
    (TRUE.StackTest.5)     // We jumped here if TRUE
        @SP    
        A=M-1  // make A equal the number at M[SP]-1
        M=-1   // -1 is TRUE
    (ENDIF.StackTest.5)    // Place to jump after FALSE
               
               // push constant 16
        @16     // C_PUSH constant
        D=A    // Load the const into D for later
        @SP    // get stack pointer SP
        A=M    // A will get value of M[SP]
        M=D    // Store M[M[SP]] into D
        
        @SP    // Increment Stack Pointer
        M=M+1  // M[SP] will equal M[SP] + 1
               
               // push constant 17
        @17     // C_PUSH constant
        D=A    // Load the const into D for later
        @SP    // get stack pointer SP
        A=M    // A will get value of M[SP]
        M=D    // Store M[M[SP]] into D
        
        @SP    // Increment Stack Pointer
        M=M+1  // M[SP] will equal M[SP] + 1
               
               // eq
        @SP    // Decrement Stack Pointer
        M=M-1  // M[SP] will equal M[SP] - 1
        
        @SP    
        A=M    // make A equal the number at M[SP]
        D=M    // D will get data at M[M[SP]] (y)
        @SP    // Point to SP
        A=M-1  // A = M[SP]-1 (1 before current stack)
        D=M-D  // D = M[M[SP]-1](x) - D(y)
        @TRUE.StackTest.8  // LOAD The address of True, to skip false
        D;JEQ  // IF x - y = 0 then x == y
        @SP    
        A=M-1  // make A equal the number at M[SP]-1
        M=0    // FALSE is the answer if we reached here
        @ENDIF.StackTest.8 // JUMP past true
        0;JMP  
    (TRUE.StackTest.8)     // We jumped here if TRUE
        @SP    
        A=M-1  // make A equal the number at M[SP]-1
        M=-1   // -1 is TRUE
    (ENDIF.StackTest.8)    // Place to jump after FALSE
               
               // push constant 892
        @892     // C_PUSH constant
        D=A    // Load the const into D for later
        @SP    // get stack pointer SP
        A=M    // A will get value of M[SP]
        M=D    // Store M[M[SP]] into D
        
        @SP    // Increment Stack Pointer
        M=M+1  // M[SP] will equal M[SP] + 1
               
               // push constant 891
        @891     // C_PUSH constant
        D=A    // Load the const into D for later
        @SP    // get stack pointer SP
        A=M    // A will get value of M[SP]
        M=D    // Store M[M[SP]] into D
        
        @SP    // Increment Stack Pointer
        M=M+1  // M[SP] will equal M[SP] + 1
               
               // lt
        @SP    // Decrement Stack Pointer
        M=M-1  // M[SP] will equal M[SP] - 1
        
        @SP    
        A=M    // make A equal the number at M[SP]
        D=M    // D will get data at M[M[SP]] (y)
        @SP    // Point to SP
        A=M-1  // A = M[SP]-1 (1 before current stack)
        D=M-D  // D = M[M[SP]-1](x) - D(y)
        @TRUE.StackTest.11  // LOAD The address of True, to skip false
        D;JLT  // IF x - y = is lesser than 0 then x < y
        @SP    
        A=M-1  // make A equal the number at M[SP]-1
        M=0    // FALSE is the answer if we reached here
        @ENDIF.StackTest.11 // JUMP past true
        0;JMP  
    (TRUE.StackTest.11)     // We jumped here if TRUE
        @SP    
        A=M-1  // make A equal the number at M[SP]-1
        M=-1   // -1 is TRUE
    (ENDIF.StackTest.11)    // Place to jump after FALSE
               
               // push constant 891
        @891     // C_PUSH constant
        D=A    // Load the const into D for later
        @SP    // get stack pointer SP
        A=M    // A will get value of M[SP]
        M=D    // Store M[M[SP]] into D
        
        @SP    // Increment Stack Pointer
        M=M+1  // M[SP] will equal M[SP] + 1
               
               // push constant 892
        @892     // C_PUSH constant
        D=A    // Load the const into D for later
        @SP    // get stack pointer SP
        A=M    // A will get value of M[SP]
        M=D    // Store M[M[SP]] into D
        
        @SP    // Increment Stack Pointer
        M=M+1  // M[SP] will equal M[SP] + 1
               
               // lt
        @SP    // Decrement Stack Pointer
        M=M-1  // M[SP] will equal M[SP] - 1
        
        @SP    
        A=M    // make A equal the number at M[SP]
        D=M    // D will get data at M[M[SP]] (y)
        @SP    // Point to SP
        A=M-1  // A = M[SP]-1 (1 before current stack)
        D=M-D  // D = M[M[SP]-1](x) - D(y)
        @TRUE.StackTest.14  // LOAD The address of True, to skip false
        D;JLT  // IF x - y = is lesser than 0 then x < y
        @SP    
        A=M-1  // make A equal the number at M[SP]-1
        M=0    // FALSE is the answer if we reached here
        @ENDIF.StackTest.14 // JUMP past true
        0;JMP  
    (TRUE.StackTest.14)     // We jumped here if TRUE
        @SP    
        A=M-1  // make A equal the number at M[SP]-1
        M=-1   // -1 is TRUE
    (ENDIF.StackTest.14)    // Place to jump after FALSE
               
               // push constant 891
        @891     // C_PUSH constant
        D=A    // Load the const into D for later
        @SP    // get stack pointer SP
        A=M    // A will get value of M[SP]
        M=D    // Store M[M[SP]] into D
        
        @SP    // Increment Stack Pointer
        M=M+1  // M[SP] will equal M[SP] + 1
               
               // push constant 891
        @891     // C_PUSH constant
        D=A    // Load the const into D for later
        @SP    // get stack pointer SP
        A=M    // A will get value of M[SP]
        M=D    // Store M[M[SP]] into D
        
        @SP    // Increment Stack Pointer
        M=M+1  // M[SP] will equal M[SP] + 1
               
               // lt
        @SP    // Decrement Stack Pointer
        M=M-1  // M[SP] will equal M[SP] - 1
        
        @SP    
        A=M    // make A equal the number at M[SP]
        D=M    // D will get data at M[M[SP]] (y)
        @SP    // Point to SP
        A=M-1  // A = M[SP]-1 (1 before current stack)
        D=M-D  // D = M[M[SP]-1](x) - D(y)
        @TRUE.StackTest.17  // LOAD The address of True, to skip false
        D;JLT  // IF x - y = is lesser than 0 then x < y
        @SP    
        A=M-1  // make A equal the number at M[SP]-1
        M=0    // FALSE is the answer if we reached here
        @ENDIF.StackTest.17 // JUMP past true
        0;JMP  
    (TRUE.StackTest.17)     // We jumped here if TRUE
        @SP    
        A=M-1  // make A equal the number at M[SP]-1
        M=-1   // -1 is TRUE
    (ENDIF.StackTest.17)    // Place to jump after FALSE
               
               // push constant 32767
        @32767     // C_PUSH constant
        D=A    // Load the const into D for later
        @SP    // get stack pointer SP
        A=M    // A will get value of M[SP]
        M=D    // Store M[M[SP]] into D
        
        @SP    // Increment Stack Pointer
        M=M+1  // M[SP] will equal M[SP] + 1
               
               // push constant 32766
        @32766     // C_PUSH constant
        D=A    // Load the const into D for later
        @SP    // get stack pointer SP
        A=M    // A will get value of M[SP]
        M=D    // Store M[M[SP]] into D
        
        @SP    // Increment Stack Pointer
        M=M+1  // M[SP] will equal M[SP] + 1
               
               // gt
        @SP    // Decrement Stack Pointer
        M=M-1  // M[SP] will equal M[SP] - 1
        
        @SP    
        A=M    // make A equal the number at M[SP]
        D=M    // D will get data at M[M[SP]] (y)
        @SP    // Point to SP
        A=M-1  // A = M[SP]-1 (1 before current stack)
        D=M-D  // D = M[M[SP]-1](x) - D(y)
        @TRUE.StackTest.20  // LOAD The address of True, to skip false
        D;JGT  // IF x - y = is greater than 0 then x > y
        @SP    
        A=M-1  // make A equal the number at M[SP]-1
        M=0    // FALSE is the answer if we reached here
        @ENDIF.StackTest.20 // JUMP past true
        0;JMP  
    (TRUE.StackTest.20)     // We jumped here if TRUE
        @SP    
        A=M-1  // make A equal the number at M[SP]-1
        M=-1   // -1 is TRUE
    (ENDIF.StackTest.20)    // Place to jump after FALSE
               
               // push constant 32766
        @32766     // C_PUSH constant
        D=A    // Load the const into D for later
        @SP    // get stack pointer SP
        A=M    // A will get value of M[SP]
        M=D    // Store M[M[SP]] into D
        
        @SP    // Increment Stack Pointer
        M=M+1  // M[SP] will equal M[SP] + 1
               
               // push constant 32767
        @32767     // C_PUSH constant
        D=A    // Load the const into D for later
        @SP    // get stack pointer SP
        A=M    // A will get value of M[SP]
        M=D    // Store M[M[SP]] into D
        
        @SP    // Increment Stack Pointer
        M=M+1  // M[SP] will equal M[SP] + 1
               
               // gt
        @SP    // Decrement Stack Pointer
        M=M-1  // M[SP] will equal M[SP] - 1
        
        @SP    
        A=M    // make A equal the number at M[SP]
        D=M    // D will get data at M[M[SP]] (y)
        @SP    // Point to SP
        A=M-1  // A = M[SP]-1 (1 before current stack)
        D=M-D  // D = M[M[SP]-1](x) - D(y)
        @TRUE.StackTest.23  // LOAD The address of True, to skip false
        D;JGT  // IF x - y = is greater than 0 then x > y
        @SP    
        A=M-1  // make A equal the number at M[SP]-1
        M=0    // FALSE is the answer if we reached here
        @ENDIF.StackTest.23 // JUMP past true
        0;JMP  
    (TRUE.StackTest.23)     // We jumped here if TRUE
        @SP    
        A=M-1  // make A equal the number at M[SP]-1
        M=-1   // -1 is TRUE
    (ENDIF.StackTest.23)    // Place to jump after FALSE
               
               // push constant 32766
        @32766     // C_PUSH constant
        D=A    // Load the const into D for later
        @SP    // get stack pointer SP
        A=M    // A will get value of M[SP]
        M=D    // Store M[M[SP]] into D
        
        @SP    // Increment Stack Pointer
        M=M+1  // M[SP] will equal M[SP] + 1
               
               // push constant 32766
        @32766     // C_PUSH constant
        D=A    // Load the const into D for later
        @SP    // get stack pointer SP
        A=M    // A will get value of M[SP]
        M=D    // Store M[M[SP]] into D
        
        @SP    // Increment Stack Pointer
        M=M+1  // M[SP] will equal M[SP] + 1
               
               // gt
        @SP    // Decrement Stack Pointer
        M=M-1  // M[SP] will equal M[SP] - 1
        
        @SP    
        A=M    // make A equal the number at M[SP]
        D=M    // D will get data at M[M[SP]] (y)
        @SP    // Point to SP
        A=M-1  // A = M[SP]-1 (1 before current stack)
        D=M-D  // D = M[M[SP]-1](x) - D(y)
        @TRUE.StackTest.26  // LOAD The address of True, to skip false
        D;JGT  // IF x - y = is greater than 0 then x > y
        @SP    
        A=M-1  // make A equal the number at M[SP]-1
        M=0    // FALSE is the answer if we reached here
        @ENDIF.StackTest.26 // JUMP past true
        0;JMP  
    (TRUE.StackTest.26)     // We jumped here if TRUE
        @SP    
        A=M-1  // make A equal the number at M[SP]-1
        M=-1   // -1 is TRUE
    (ENDIF.StackTest.26)    // Place to jump after FALSE
               
               // push constant 57
        @57     // C_PUSH constant
        D=A    // Load the const into D for later
        @SP    // get stack pointer SP
        A=M    // A will get value of M[SP]
        M=D    // Store M[M[SP]] into D
        
        @SP    // Increment Stack Pointer
        M=M+1  // M[SP] will equal M[SP] + 1
               
               // push constant 31
        @31     // C_PUSH constant
        D=A    // Load the const into D for later
        @SP    // get stack pointer SP
        A=M    // A will get value of M[SP]
        M=D    // Store M[M[SP]] into D
        
        @SP    // Increment Stack Pointer
        M=M+1  // M[SP] will equal M[SP] + 1
               
               // push constant 53
        @53     // C_PUSH constant
        D=A    // Load the const into D for later
        @SP    // get stack pointer SP
        A=M    // A will get value of M[SP]
        M=D    // Store M[M[SP]] into D
        
        @SP    // Increment Stack Pointer
        M=M+1  // M[SP] will equal M[SP] + 1
               
               // add
        @SP    // Decrement Stack Pointer
        M=M-1  // M[SP] will equal M[SP] - 1
        
        @SP    
        A=M    // make A equal the number at M[SP]
        D=M    // D will get data at M[M[SP]] (y)
        @SP    // Point to SP
        A=M-1  // A = M[SP]-1 (1 before current stack)
        M=D+M  // M[M[SP]-1](x) = D(y) + M[M[SP]-1](x)
               
               // push constant 112
        @112     // C_PUSH constant
        D=A    // Load the const into D for later
        @SP    // get stack pointer SP
        A=M    // A will get value of M[SP]
        M=D    // Store M[M[SP]] into D
        
        @SP    // Increment Stack Pointer
        M=M+1  // M[SP] will equal M[SP] + 1
               
               // sub
        @SP    // Decrement Stack Pointer
        M=M-1  // M[SP] will equal M[SP] - 1
        
        @SP    
        A=M    // make A equal the number at M[SP]
        D=M    // D will get data at M[M[SP]] (y)
        @SP    // Point to SP
        A=M-1  // A = M[SP]-1 (1 before current stack)
        M=M-D  // M[M[SP]-1](x) = D(y) - M[M[SP]-1](x)
               
               // neg
        @SP    
        A=M-1  // make A equal the number at M[SP] -1
        M=-M   // M[M[SP]-1](x) = -M[M[SP]-1](x)
               
               // and
        @SP    // Decrement Stack Pointer
        M=M-1  // M[SP] will equal M[SP] - 1
        
        @SP    
        A=M    // make A equal the number at M[SP]
        D=M    // D will get data at M[M[SP]] (y)
        @SP    // Point to SP
        A=M-1  // A = M[SP]-1 (1 before current stack)
        M=D&M  // M[M[SP]-1](x) = D(y) & M[M[SP]-1](x)
               
               // push constant 82
        @82     // C_PUSH constant
        D=A    // Load the const into D for later
        @SP    // get stack pointer SP
        A=M    // A will get value of M[SP]
        M=D    // Store M[M[SP]] into D
        
        @SP    // Increment Stack Pointer
        M=M+1  // M[SP] will equal M[SP] + 1
               
               // or
        @SP    // Decrement Stack Pointer
        M=M-1  // M[SP] will equal M[SP] - 1
        
        @SP    
        A=M    // make A equal the number at M[SP]
        D=M    // D will get data at M[M[SP]] (y)
        @SP    // Point to SP
        A=M-1  // A = M[SP]-1 (1 before current stack)
        M=D|M  // M[M[SP]-1](x) = D(y) | M[M[SP]-1](x)
               
               // not
        @SP    
        A=M-1  // make A equal the number at M[SP] -1
        M=!M   // M[M[SP]-1](x) = !M[M[SP]-1](x)